# FROM docker:20-dind
FROM node:16-alpine

RUN apk add --update docker openrc
RUN rc-update add docker boot

COPY docker-entrypoint.sh /usr/local/bin/
ENTRYPOINT ["docker-entrypoint.sh"]

CMD [ "node" ]
